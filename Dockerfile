# vi Dockerfile
FROM jupyter/datascience-notebook:python-3.10
USER root
# poetryのインストール先の指定
ENV POETRY_HOME=/opt/poetry
RUN curl -sSL https://install.python-poetry.org/ | python -
RUN cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    # 仮想環境を作成しない設定(コンテナ前提のため，仮想環境を作らない)
    poetry config virtualenvs.create false && \
    poetry new myProject --name src
RUN pip install --upgrade pip


# ユーザー変更
USER jovya


